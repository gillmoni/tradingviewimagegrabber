#!/usr/bin/python3
import requests, bs4 #To get values from HTML source code.
import datetime, dateparser #To modify date values
import webbrowser, sys, pyperclip #Open Links.
import pyautogui #Automate screenshots and move movements.
import time, re
import os.path
from PIL import Image #Stich before and after images.

 
##########################################################################################################################################
##########################################################################################################################################
#def read_file              : Reads trade_links from file and calls 'image_name_generator'.
#def image_name_generator   : Generates before and after names for PNG files and calls 'open_page'.
#def open_page              : Opens the page and saves images, also uses 'merge_images' to merge two images.
#def merge_images
##########################################################################################################################################
##########################################################################################################################################
def read_file(file_name): 
    '''
        Traverse file line by line.
        :param file_name: File that contains list of TradingView links.
        return: Nothing.
    '''
    if not file_name:
        print ("No file specified. exit()")
        #file_name = 'tradeLinks.cfg'
        exit()
    
    if os.path.isfile(file_name): #If file exits, stich. Else, do nothing.
        '''Assign Default file'''
        file_name = 'tradeLinks.cfg'
    

    trade_addresses = [line.rstrip('\n') for line in open('tradeLinks.cfg')]
    s_no = int(1)
    for link_address in trade_addresses:
        '''
            Check for link sanitization, sometimes link is not in expected format which can give a error later.
            Link Format for different types
            #ScreenShot#        https://www.tradingview.com/x/8bR5oobZ/
            #Publushed Idea#    https://www.tradingview.com/chart/EURCAD/EibW1gwQ-EC-1H-Short/
        '''
        if "chart" in link_address: #If 'chart' doesn't exist in link, skip.
            image_name_generator(s_no, link_address)
            s_no += 1 #Increment. S_No counter.

            #return link_address
        
##########################################################################################################################################
########################################################################################################################################## 
def image_name_generator(s_no,link_address): 
    ''' 
        Find name for files to be saved.
    '''
    if link_address:
        res = requests.get(link_address)
        soup = bs4.BeautifulSoup(res.text, "html5lib")
        #<title>GJ 1H Long — British Pound/Japanese Yen (FX:GBPJPY) / 2017-05-15 — gill.moni | TradingView</title>
        spanElems = soup.findAll(['title'])
        '''
            Input # From View Source
            #Step 1
            [<title>GJ 1H Long — British Pound/Japanese Yen (FX:GBPJPY) / 2017-05-15 — gill.moni | TradingView</title>]
             
            #Step 2
            British Pound/Japanese Yen (FX:GBPJPY) / 2017-05-15 
             
            #Step 3
            Name : GJ 1H Long 
            Pair : GBPJPY
            Date : 2017-05-15
        '''
             
        string =  str(spanElems[0])
        title_string = string.split('—')[1]
        title_string_elements =  title_string.split(' ')
             
        ''' 
            Debug Statements
        '''
        #print ("String =", string)
        print ("Input =", string.split('|')[0].split('>')[1])
        print ("Title =", title_string)
        #print ("Title Elements =", title_string_elements)
            
        '''Finding Name and other details from Title
            Title
            Trade Scale
            Trade Direction
            Pair Traded
        '''
             
        #Completed: Now implemented with Regex. FX:[A-Z][A-Z][A-Z][A-Z][A-Z][A-Z]
        trade_name = string.split('—')[0].split(">")[1]
             
        trade_time_frame = trade_name.split(" ")[1]
        trade_direction = trade_name.split(" ")[2]
                 
        '''
            Individual Elements
        '''
        trade_date = title_string_elements[-2]
             
        #Title =  Australian Dollar/New Zealand Dollar (FX:AUDNZD) / 2017-04-06 
        #trade_pair = (title_string_elements[4][4:])[:6] #Depreciated. This wasn't readong New Zealand Dollar properly.
        trade_pair = str(re.findall(r'\(FX\:(.*?)\)',title_string)[0]) #This uses regex.
         
        print ("Pair :", trade_pair)
        print ("Trade Scale :", trade_time_frame)
        print ("Trade Direction :", trade_direction)
        print ("Name :", trade_name)
        print ("Date :", trade_date)
             
        '''
            Convert standard date format to human readable.
        '''
        date_object = dateparser.parse(trade_date)
        date_object_a = date_object.strftime("%d-%b-%y")
        date_object_b = date_object.strftime("%b-%d-%y")
        
        #date_object = date_object_a
        date_object = date_object_b

        print ("Date Text :", date_object)
             
        '''
            Get the final file names.
        s_no = ''
        '''
        t_prefix = str(s_no)+"."
        t_suffix = '.png'
        file_name_a = t_prefix+trade_pair+'_'+trade_time_frame+'_'+trade_direction+'_'+date_object
        file_name_b = date_object+'_'+trade_pair+'_'+trade_time_frame+'_'+trade_direction
         
        #file_name = file_name_a
        file_name = file_name_b
             
        file_name_before = file_name+'_before'+t_suffix
        file_name_after = file_name+'_after'+t_suffix
             
        print("Before Name - ", file_name_before)
        print("After Name - ", file_name_after)
        print("----------------------------------------------------------------------\n")
             
        '''
        Call function to open link_address and automate GUI.
        '''
        open_page(link_address, file_name_before, file_name_after, screenshot_region, reset_cordinates, play_cordinates, null_cordinates)
             
        if os.path.isfile(file_name_before): #If file exits, stich. Else, do nothing.
            merged = merge_images(file_name_before, file_name_after) 
            merged.save(file_name+t_suffix)
             
        #s_no += 1 #Increment. S_No counter.
        #exit()
        #return (file_name_before, file_name_after)
    
        
##########################################################################################################################################
##########################################################################################################################################
def merge_images(before_file, after_file):
    '''
        Merge two images into one, displayed side by side
        :param before_file: path to first image file
        :param after_file: path to second image file
        :return: the merged Image object
    '''
    image1 = Image.open(before_file)
    image2 = Image.open(after_file)
 
    (width1, height1) = image1.size
    (width2, height2) = image2.size
 
    result_width = width1 + width2
    result_height = max(height1, height2)
 
    result = Image.new('RGB', (result_width, result_height))
    result.paste(im=image1, box=(0, 0))
    result.paste(im=image2, box=(width1, 0))
    return result
 
##########################################################################################################################################
##########################################################################################################################################
def open_page(address, file_name_before, file_name_after, screenshot_region, reset_cordinates, play_cordinates, null_cordinates):
    '''
        Opens link provided, inspects link and then takes 
        :param adress: TradingView link to perform operation on.
        return: Nothing.
        result: PNG file saved on disk.
    '''
    '''
    if len(sys.argv) > 1:
        #Get address from Command Line
        print ("Opening Page". address)
        #address = ' '.join(sys.argv[:1])
    else:
        #Get address from clipboard.
        #address = pyperclip.paste()
        print ("Doing nothing")
     
    print("Opening ", address)
   '''

    #TBD: Check if any argument is null, if yes, exit.

    webbrowser.open(address)
    #page = webbrowser.open(address)
    #Sleep 10 seconds (for page to load)
    time.sleep(10)
     
    '''     
        Move mouse to reset cordinates and click.
        Allows to set all charts on same resolution.
    '''
    pyautogui.moveTo(reset_cordinates, duration=0.40)
    pyautogui.click(reset_cordinates);
    time.sleep(.25)
     
    '''     
        Move mouse to NULL location. Doesnt come in screenshot
    '''
    pyautogui.moveTo(null_cordinates);
     
    '''     
        Take screenshot and save as before file.
    '''
    im = pyautogui.screenshot(region=screenshot_region)
    im.save(file_name_before)
     
    #Sleep 10 seconds (for page to load)
    time.sleep(10)
     
    '''     
        Move mouse to play cordinates and click.
        Allows to set all charts on same resolution.
    '''
    pyautogui.moveTo(play_cordinates, duration=0.40)
    pyautogui.click(play_cordinates);
    time.sleep(.25)
    #Move to NULL location. Doesnt come in screenshot
    pyautogui.moveTo(null_cordinates);
     
    '''     
        Take screenshot and save as after file.
    '''
    time.sleep(5)
    im = pyautogui.screenshot(region=screenshot_region)
    im.save(file_name_after)
     
    '''     
        Close web-browser window.
    '''
    pyautogui.hotkey('ctrl', 'w')
    ''' 
        Function ends.
    '''

##########################################################################################################################################
#Main#
##########################################################################################################################################
'''
    Co-ordinates and other parameters to be used for screenshot PyAutoGUI
'''
screen_width, screen_height = pyautogui.size()
screenshot_region = (122,197, 1630, 856) 
reset_cordinates = (935, 935)
play_cordinates = (1593, 635)
null_cordinates = (150, 150)

if len(sys.argv) > 1:
    #Open file
    file_argument = sys.argv[1]
    print ("File is", file_argument)
else:
    file_argument = 'tradeLinks.cfg'
    #Get file_argument from clipboard.
    #file_argument = pyperclip.paste()
    print ("Default File is", file_argument)    
#Call function to read links from a file_name
read_file(file_argument)

##########################################################################################################################################
#Main:END#
##########################################################################################################################################