# README #

### What is this repository for? ###
This tool facilitates journaling process for Tradingview published ideas, and takes three screenshots (png) automatically

* Before
* After
* Before and After comparison in a single image.


* Version 1.0

### How do I get set up? ###
Modules needed

```
#!shell

sudo pip3 install requests, bs4 ,datetime, dateparser, webbrowser, sys, pyperclip, pyautogui, time, re, os.path, PIL
```
```
#!shell

sudo pip install requests, bs4 ,datetime, dateparser, webbrowser, sys, pyperclip, pyautogui, time, re, os.path, PIL
```

* Summary of set up and Dependencies
Install above dependencies.

* Configuration
A .cfg file containing links line by line. 

Note: Any Chrome/Firefox multiple link grabber extension would do it.

* How to run tests

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### ScreenShots ###
[Full Trade Log at Dropbox](https://www.dropbox.com/sh/uw0mtgr38nukx19/AAAeq7vXL8jzFf5VOwl3fgv7a?dl=0)

![1.AUDJPY_1H_Short_04-Jan-17_before.png](https://bitbucket.org/repo/z8Xj69q/images/1472616164-1.AUDJPY_1H_Short_04-Jan-17_before.png)
![1.AUDJPY_1H_Short_04-Jan-17_after.png](https://bitbucket.org/repo/z8Xj69q/images/1474149851-1.AUDJPY_1H_Short_04-Jan-17_after.png)
![1.AUDJPY_1H_Short_04-Jan-17.png](https://bitbucket.org/repo/z8Xj69q/images/542789684-1.AUDJPY_1H_Short_04-Jan-17.png)

### Video ###
![giphy.gif](https://bitbucket.org/repo/z8Xj69q/images/1691574729-giphy.gif)